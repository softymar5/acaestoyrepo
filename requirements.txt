1ro. asegurarse de tener instalados:
python==3.7.0
django>=2.1.7
pylint==2.3.1
pillow==5.4.1
sqlite==3.25.3

Luego de instalados, para correr el proyecto, desde una terminal de Windows, moverse a la carpeta donde se encuentra manage.py (..\AcaEstoy\manage.py), e ingresar:
>python manage.py runserver

Se va a generar un servidor virtual que permitirá correr el sitio en cualquier navegador de internet. Simplemente hay que hacer ctrl-clic sobre el link http://127.0.0.1:8000/
