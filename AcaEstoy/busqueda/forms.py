from django import forms

class BusquedaForm(forms.Form):
    extraviada = forms.BooleanField(label="Extraviada", required=False)
    encontrada = forms.BooleanField(label= "Encontrada", required=False)
    departamento = forms.CharField(label="Departamento", required=False)
    especie = forms.CharField(label="Especie", required=False)
    color_primario = forms.CharField(label="Color", required=False)

