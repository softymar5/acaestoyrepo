from django.shortcuts import render, redirect
from .forms import BusquedaForm
from django.urls import reverse

# Create your views here.
def busqueda(request):
    busqueda_form = BusquedaForm()

    if request.method == "POST":
        busqueda_form = BusquedaForm(data=request.POST)
        if busqueda_form.is_valid():
            extraviada = request.POST.get('extraviada','')
            encontrada = request.POST.get('encontrada','')
            departamento = request.POST.get('departamento', '')
            especie = request.POST.get('especie', '')
            color_primario = request.POST.get('color_primario', '')
            #suponemos que está ok y redireccionamos
            return redirect(reverse('busqueda')+"?ok")

    return render(request, 'busqueda/busqueda.html',{'form':busqueda_form})
