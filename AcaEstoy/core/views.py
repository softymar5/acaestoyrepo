from django.shortcuts import render, HttpResponse
from .models import Animal
from .models import Perro
from .models import Gato
from .models import Otro


# Create your views here.
def index(request):
    return render (request, "app/index.html")

def reporte(request):
    return render (request, "app/reporte.html")

def reporte_exito(request):
    return render (request, "app/reporte_exito.html")

def ingreso(request):
    return render (request, "app/ingreso.html")

def registro(request):
    return render (request, "app/registro.html")

def busqueda(request):
    return render (request, "app/busqueda.html")

def mapa(request):
    return render (request, "app/mapa.html")

def reporte_ver(request):
    return render (request, "app/reporte_ver.html")
