from django.contrib import admin
from django.urls import reverse_lazy

# Register your models here.

#traigo el modelo
from core.models import Usuario
from core.models import Perro
from core.models import Gato
from core.models import Otro


#lo paso al backend para CRUD
admin.site.register(Usuario)
admin.site.register(Perro)
admin.site.register(Gato)
admin.site.register(Otro)
