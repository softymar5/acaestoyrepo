from django.db import models

class Animal(models.Model):

    especies = (('', 'Seleccionar'), ('PE', 'Perro'), ('GA', 'Gato'), ('OT', 'Otros'),)
    sexos = (('', 'Seleccionar'), ('MA', 'Macho'), ('HE', 'Hembra'), ('DE', 'Desconocido'),)
    edades = (('', 'Seleccionar'), ('CA', 'Cachorro'), ('JO', 'Joven'), ('AD', 'Adulto'), ('VI', 'Viejo'),)
    tamaños = (('', 'Seleccionar'), ('PQ', 'Pequeño (menos de 10 kg)'), ('MD', 'Mediano(entre 10 y 25 kg)'),
            ('GD', 'Grande(entre 25 y 45 kg)'), ('XG', 'Extra grande(más de 45 kg)'),)
    colores = (('', 'Seleccionar'), ('BL', 'Blanco'), ('NG', 'Negro'), ('GR', 'Gris o plateado'), ('BG', 'Crema, beige, marrón claro'),
            ('MR', 'Marrón oscuro'), ('RJ', 'Rojizo'), ('AM', 'Amarillo, rubio, dorado'), ('AZ', 'Azul'), ('VR', 'Verde'),
            ('AN', 'Anaranjado'), ('OT', 'Otro'), ('NA', 'No aplica'),)
    tipos_repos = (('EX', 'Mascota extraviada'), ('EN', 'Mascota encontrada'),)
    dptos = (('', 'Seleccionar'), ('AR', "Artigas"), ('CA', 'Canelones'), ('CL', 'Cerro Largo'), ('CO', 'Colonia'),
            ('DU', 'Durazno'), ('FL', 'Flores'), ('FA','Florida'), ('LA', 'Lavalleja'),
            ('MA', 'Maldonado'), ('MO','Montevideo'), ('PA', 'Paysandu'), ('RN','Rio Negro'),
            ('RI','Rivera'), ('RO', 'Rocha'), ('SA','Salto'), ('SJ','San Jose'), ('SO', 'Soriano'),
            ('TA', 'Tacuarembo'), ('TT', 'Treinta y Tres'),)
    localidades = (
        ('', 'Seleccionar'),
        ('Artigas', (
            ('Artigas', 'Artigas'), ('Baltasar Brum', 'Baltasar Brum'), ('Bella Union', 'Bella Unión'), ('Bernabe Rivera', 'Bernabé Rivera'),
            ('Cainsa', 'Cainsa'), ('Calnu', 'Calnu'), ('Cerro Ejido', 'Cerro Ejido'), ('Cerro San Eugenio', 'Cerro San Eugenio'),
            ('Cerro Signorelli (El Mirador)', 'Cerro Signorelli (El Mirador)'), ('Colonia Palma', 'Colonia Palma'), ('Coronado', 'Coronado'),
            ('Cuareim', 'Cuareim'), ('Cuaro', 'Cuaró'), ('Diego Lamas', 'Diego Lamas'), ('Franquia', 'Franquia'),
            ('Javier de Viana', 'Javier de Viana'), ('La Bolsa', 'La Bolsa'), ('Las Piedras Art', 'Las Piedras'),
            ('Mones Quintela', 'Mones Quintela'), ('Paso Campamento', 'Paso Campamento'), ('Paso Farias', 'Paso Farías'),
            ('Pintadito', 'Pintadito'), ('Port. de Hierro y Campodonico', 'Port. de Hierro y Campodónico'),
            ('Rincon de Pacheco', 'Rincón de Pacheco'), ('Sequeira', 'Sequeira'), ('Tomas Gomensoro', 'Tomás Gomensoro'),
            ('Topador', 'Topador'),
        )),
        ('Canelones', (
            ('Aeropuerto Internacional de Carrasco', 'Aeropuerto Internacional de Carrasco'), ('Aguas Corrientes', 'Aguas Corrientes'),
            ('Altos de la Tahona', 'Altos de la Tahona'), ('Araminda', 'Araminda'), ('Argentino', 'Argentino'), ('Atlantida', 'Atlántida'),
            ('Barra de Carrasco', 'Barra de Carrasco'), ('Barrio Copola', 'Barrio Cópola'), ('Barrio Remanso', 'Barrio Remanso'),
            ('Barros Blancos', 'Barros Blancos'), ('Bello Horizonte', 'Bello Horizonte'), ('Biarritz', 'Biarritz'), ('Bolivar', 'Bolívar'),
            ('Campo Militar', 'Campo Militar'), ('Canelones', 'Canelones'), ('Capilla de Cella', 'Capilla de Cella'), ('Carmel', 'Carmel'),
            ('Castellanos', 'Castellanos'), ('Cerrillos', 'Cerrillos'), ('City Golf', 'City Golf'),
            ('Colinas de Carrasco', 'Colinas de Carrasco'), ('Colinas de Solymar', 'Colinas de Solymar'),
            ('Colonia Nicolich', 'Colonia Nicolich'), ('Costa Azul', 'Costa Azul'), ('Costa y Guillamon', 'Costa y Guillamón'),
            ('Cruz de los Caminos', 'Cruz de los Caminos'), ('Cuchilla Alta', 'Cuchilla Alta'), ('Cumbres de Carrasco', 'Cumbres de Carrasco'),
            ('Dr. Francisco Soca', 'Dr. Francisco Soca'), ('El Bosque', 'El Bosque'), ('El Galeon', 'El Galeón'), ('El Pinar', 'El Pinar'),
            ('Empalme Olmos', 'Empalme Olmos'), ('Estacion Atlantida', 'Estación Atlántida'), ('Estacion La Floresta', 'Estación La Floresta'),
            ('Estacion Migues', 'Estación Migues'), ('Estacion Pedrera', 'Estación Pedrera'),
            ('Estacion Piedras de Afilar', 'Estación Piedras de Afilar'), ('Estacion Tapia', 'Estación Tapia'),
            ('Estanque de Pando', 'Estanque de Pando'), ('Fortin de Santa Rosa', 'Fortín de Santa Rosa'),
            ('Fracc. Cno. Andaluz y R.84', 'Fracc. Cno. Andaluz y R.84'), ('Fracc. Progreso', 'Fracc. Progreso'),
            ('Fracc. sobre Ruta 74', 'Fracc. sobre Ruta 74'), ('Guazuvira', 'Guazuvirá'), ('Haras del Lago', 'Haras del Lago'),
            ('Instituto Adventista', 'Instituto Adventista'), ('Jardines de Pando', 'Jardínes de Pando'), ('Jaureguiberry', 'Jaureguiberry'),
            ('Joaquin Suarez', 'Joaquín Suarez'), ('Juanico', 'Juanicó'), ('La Asuncion', 'La Asunción'), ('La Floresta', 'La Floresta'),
            ('La Lucha', 'La Lucha'), ('La Montañesa', 'La Montañesa'), ('La Paz', 'La Paz'), ('La Tuna', 'La Tuna'), ('Lagomar', 'Lagomar'),
            ('Las Higueritas', 'Las Higueritas'), ('Las Piedras', 'Las Piedras'), ('Las Toscas', 'Las Toscas'),
            ('Lomas de Carrasco', 'Lomas de Carrasco'), ('Lomas de Solymar', 'Lomas de Solymar'), ('Los Titanes', 'Los Titanes'),
            ('Marindia', 'Marindia'), ('Migues', 'Migues'), ('Montes', 'Montes'), ('Neptunia', 'Neptunia'), ('Olmos', 'Olmos'),
            ('Pando', 'Pando'), ('Parada Cabrera', 'Parada Cabrera'), ('Parque Carrasco', 'Parque Carrasco'),
            ('Parque del Plata', 'Parque del Plata'), ('Paso Carrasco', 'Paso Carrasco'), ('Paso de La Cadena', 'Paso de La Cadena'),
            ('Paso de Pache', 'Paso de Pache'), ('Paso Espinosa', 'Paso Espinosa'), ('Paso Palomeque', 'Paso Palomeque'),
            ('Piedra del Toro', 'Piedra del Toro'), ('Piedras de Afilar', 'Piedras de Afilar'), ('Pinamar - Pinepark', 'Pinamar - Pinepark'),
            ('Progreso', 'Progreso'), ('Quinta Los Horneros', 'Quinta Los Horneros'), ('Quintas del Bosque', 'Quintas del Bosque'),
            ('Salinas', 'Salinas'), ('San Antonio', 'San Antonio'), ('San Bautista', 'San Bautista'), ('San Jacinto', 'San Jacinto'),
            ('San Jose de Carrasco', 'San José de Carrasco'), ('San Luis', 'San Luis'), ('San Ramon', 'San Ramón'),
            ('Santa Ana', 'Santa Ana'), ('Santa Lucia', 'Santa Lucía'), ('Santa Lucia del Este', 'Santa Lucía del Este'),
            ('Santa Rosa', 'Santa Rosa'), ('Sauce', 'Sauce'), ('Seis Hermanos', 'Seis Hermanos'), ('Shangrila', 'Shangrilá'),
            ('Sofia Santos', 'Sofía Santos'), ('Solymar', 'Solymar'), ('Tala', 'Tala'), ('Toledo', 'Toledo'),
            ('Totoral del Sauce', 'Totoral del Sauce'), ('Viejo Molino San Bernardo', 'Viejo Molino San Bernardo'),
            ('Villa Aeroparque', 'Villa Aeroparque'), ('Villa Arejo', 'Villa Arejo'), ('Villa Argentina', 'Villa Argentina'),
            ('Villa Crespo y San Andres', 'Villa Crespo y San Andrés'), ('Villa El Tato', 'Villa El Tato'),
            ('Villa Felicidad', 'Villa Felicidad'), ('Villa Hadita', 'Villa Hadita'), ('Villa Juana', 'Villa Juana'),
            ('Villa Paz S.A.', 'Villa Paz S.A.'), ('Villa Porvenir', 'Villa Porvenir'), ('Villa San Cono', 'Villa San Cono'),
            ('Villa San Felipe', 'Villa San Felipe'), ('Villa San Jose', 'Villa San José'),
        )),
        ('Cerro Largo', (
            ('Acegua', 'Aceguá'), ('Arachania', 'Arachania'), ('Arbolito', 'Arbolito'), ('Arevalo', 'Arévalo'),
            ('Bañado de Medina', 'Bañado de Medina'), ('Barrio La Vinchuca', 'Barrio La Vinchuca'),
            ('Barrio Lopez Benitez', 'Barrio López Benitez'), ('Caserio Las Cañas', 'Caserío Las Cañas'), ('Centurion', 'Centurión'),
            ('Cerro de las Cuentas', 'Cerro de las Cuentas'), ('Ñangapire', 'Ñangapiré'), ('Esperanza', 'Esperanza'),
            ('Fraile Muerto', 'Fraile Muerto'), ('Getulio Vargas', 'Getulio Vargas'), ('Hipodromo', 'Hipódromo'),
            ('Isidoro Noblia', 'Isidoro Noblía'), ('La Pedrera', 'La Pedrera'), ('Lago Merin', 'Lago Merín'), ('Mangrullo', 'Mangrullo'),
            ('Melo', 'Melo'), ('Nando', 'Nando'), ('Placido Rosas', 'Plácido Rosas'), ('Poblado Uruguay', 'Poblado Uruguay'),
            ('Quebracho', 'Quebracho'), ('Ramon Trigo', 'Ramón Trigo'), ('Rio Branco', 'Río Branco'), ('Soto Goro', 'Soto Goro'),
            ('Toledo', 'Toledo'), ('Tres Islas', 'Tres Islas'), ('Tupambae', 'Tupambaé'),
        )),
        ('Colonia', (
           ('Agraciada', 'Agraciada'), ('Arrivillaga', 'Arrivillaga'), ('Artilleros', 'Artilleros'), ('Barker', 'Barker'),
           ('Blanca Arena', 'Blanca Arena'), ('Boca del Rosario', 'Boca del Rosario'), ('Brisas del Plata', 'Brisas del Plata'),
           ('Campana', 'Campana'), ('Carmelo', 'Carmelo'), ('Caserio El Cerro', 'Caserío El Cerro'), ('Cerro Carmelo', 'Cerro Carmelo'),
           ('Cerros de San Juan', 'Cerros de San Juan'), ('Chico Torino', 'Chico Torino'), ('Colonia Cosmopolita', 'Colonia Cosmopolita'),
           ('Colonia del Sacramento', 'Colonia del Sacramento'), ('Colonia Valdense', 'Colonia Valdense'), ('Conchillas', 'Conchillas'),
           ('Cufre', 'Cufré'), ('El Ensueño', 'El Ensueño'), ('El Faro', 'El Faro'), ('El Quinton', 'El Quintón'),
           ('El Semillero', 'El Semillero'), ('Estacion Estanzuela', 'Estación Estanzuela'), ('Florencio Sanchez', 'Florencio Sánchez'),
           ('Juan Carlos Caseros', 'Juan Carlos Caseros'), ('Juan Jackson', 'Juan Jackson'), ('Juan Lacaze', 'Juan Lacaze'),
           ('La Horqueta', 'La Horqueta'), ('La Paz', 'La Paz'), ('Laguna de Los Patos', 'Laguna de Los Patos'), ('Los Pinos', 'Los Pinos'),
           ('Miguelete', 'Miguelete'), ('Nueva Helvecia', 'Nueva Helvecia'), ('Nueva Palmira', 'Nueva Palmira'),
           ('Ombues de Lavalle', 'Ombúes de Lavalle'), ('Paraje Minuano', 'Paraje Minuano'), ('Paso Antolin', 'Paso Antolín'),
           ('Playa Azul', 'Playa Azul'), ('Playa Britopolis', 'Playa Britópolis'), ('Playa Fomento', 'Playa Fomento'),
           ('Playa Parant', 'Playa Parant'), ('Pueblo Gil', 'Pueblo Gil'), ('Puerto Ingles', 'Puerto Inglés'),
           ('Radial Hernandez', 'Radial Hernández'), ('Riachuelo', 'Riachuelo'), ('Rosario', 'Rosario'), ('San Pedro', 'San Pedro'),
           ('Santa Ana', 'Santa Ana'), ('Santa Regina', 'Santa Regina'), ('Tarariras', 'Tarariras'), ('Zagarzazu', 'Zagarzazu'),
        )),
        ('Durazno', (
            ('Aguas Buenas', 'Aguas Buenas'), ('Baygorria', 'Baygorria'), ('Blanquillo', 'Blanquillo'), ('Carlos Reyles', 'Carlos Reyles'),
            ('Carmen', 'Carmen'), ('Centenario', 'Centenario'), ('Cerro Chato', 'Cerro Chato'), ('Durazno', 'Durazno'),
            ('Feliciano', 'Feliciano'), ('La Paloma Dur', 'La Paloma'), ('Las Palmas', 'Las Palmas'), ('Ombues de Oribe', 'Ombúes de Oribe'),
            ('Pueblo de Alvarez', 'Pueblo de Alvarez'), ('Rossell y Rius', 'Rossell y Rius'), ('San Jorge', 'San Jorge'),
            ('Santa Bernardina', 'Santa Bernardina'), ('Sarandi del Yi', 'Sarandi del Yí'),
        )),
        ('Flores', (
            ('Andresito', 'Andresito'), ('Cerro Colorado', 'Cerro Colorado'), ('Ismael Cortinas', 'Ismael Cortinas'),
            ('Juan Jose Castro', 'Juan José Castro'), ('La Casilla', 'La Casilla'), ('Trinidad', 'Trinidad'),
        )),
        ('Florida', (
            ('25 de Agosto', '25 de Agosto'), ('25 de Mayo', '25 de Mayo'), ('Alejandro Gallinal', 'Alejandro Gallinal'),
            ('Berrondo', 'Berrondo'), ('Capilla del Sauce', 'Capilla del Sauce'), ('Cardal', 'Cardal'),
            ('Caserio La Fundacion', 'Caserío La Fundación'), ('Casupa', 'Casupá'), ('Cerro Chato', 'Cerro Chato'), ('Chamizo', 'Chamizo'),
            ('Estacion Capilla del Sauce', 'Estación Capilla del Sauce'), ('Florida', 'Florida'), ('Fray Marcos', 'Fray Marcos'),
            ('Goñi', 'Goñi'), ('Illescas', 'Illescas'), ('Independencia', 'Independencia'), ('La Cruz', 'La Cruz'), ('La Macana', 'La Macana'),
            ('Mendoza', 'Mendoza'), ('Mendoza Chico', 'Mendoza Chico'), ('Montecoral', 'Montecoral'), ('Nico Perez', 'Nico Pérez'),
            ('Pintado', 'Pintado'), ('Polanco del Yi', 'Polanco del Yí'), ('Pueblo Ferrer', 'Pueblo Ferrer'),
            ('Puntas de Maciel', 'Puntas de Maciel'), ('Reboledo', 'Reboledo'), ('San Gabriel', 'San Gabriel'),
            ('Sarandi Grande', 'Sarandí Grande'), ('Valentines', 'Valentines'),
        )),
        ('Lavalleja', (
            ('19 de Junio', '19 de Junio'), ('Aramendia', 'Aramendia'), ('Barrio La Coronilla - Ancap', 'Barrio La Coronilla - Ancap'),
            ('Blanes Viale', 'Blanes Viale'), ('Colon', 'Colón'), ('Estacion Solis', 'Estación Solís'), ('Gaetan', 'Gaetán'),
            ('Illescas', 'Illescas'), ('Jose Batlle y Ordoñez', 'José Batlle y Ordoñez'), ('Jose Pedro Varela', 'José Pedro Varela'),
            ('Mariscala', 'Mariscala'), ('Minas', 'Minas'), ('Piraraja', 'Pirarajá'), ('Polanco Norte', 'Polanco Norte'),
            ('San Francisco de las Sierras', 'San Francisco de las Sierras'), ('Solis de Mataojo', 'Solís de Mataojo'),
            ('Villa del Rosario', 'Villa del Rosario'), ('Villa Serrana', 'Villa Serrana'), ('Zapican', 'Zapicán'),
        )),
        ('Maldonado', (
            ('Aigua', 'Aiguá'), ('Arenas de Jose Ignacio', 'Arenas de José Ignacio'), ('Balneario Buenos Aires', 'Balneario Buenos Aires'),
            ('Barrio Hipodromo', 'Barrio Hipódromo'), ('Bella Vista', 'Bella Vista'), ('Canteras de Marelli', 'Canteras de Marelli'),
            ('Cerro Pelado', 'Cerro Pelado'), ('Cerros Azules', 'Cerros Azules'), ('Chihuahua', 'Chihuahua'), ('Eden Rock', 'Edén Rock'),
            ('El Chorro', 'El Chorro'), ('El Eden', 'El Edén'), ('El Quijote', 'El Quijote'), ('El Tesoro', 'El Tesoro'),
            ('Estacion Las Flores', 'Estación Las Flores'), ('Faro Jose Ignacio', 'Faro José Ignacio'), ('Garzon', 'Garzón'), ('Gerona', 'Gerona'),
            ('Gregorio Aznarez', 'Gregorio Aznarez'), ('La Barra', 'La Barra'), ('La Capuera', 'La Capuera'), ('La Sonrisa', 'La Sonrisa'),
            ('Laguna Blanca', 'Laguna Blanca'), ('Las Cumbres', 'Las Cumbres'), ('Las Flores', 'Las Flores'), ('Los Aromos', 'Los Aromos'),
            ('Los Corchos', 'Los Corchos'), ('Los Talas', 'Los Talas'), ('Maldonado', 'Maldonado'), ('Manantiales', 'Manantiales'),
            ('Nueva Carrara', 'Nueva Carrara'), ('Ocean Park', 'Ocean Park'), ('Pan de Azucar', 'Pan de Azúcar'), ('Parque Medina', 'Parque Medina'),
            ('Pinares - Las Delicias', 'Pinares - Las Delicias'), ('Piriapolis', 'Piriápolis'), ('Playa Grande', 'Playa Grande'),
            ('Playa Hermosa', 'Playa Hermosa'), ('Playa Verde', 'Playa Verde'), ('Pueblo Solis', 'Pueblo Solís'), ('Punta Ballena', 'Punta Ballena'),
            ('Punta Colorada', 'Punta Colorada'), ('Punta del Este', 'Punta del Este'), ('Punta Negra', 'Punta Negra'), ('Ruta 37 y 9', 'Ruta 37 y 9'),
            ('San Carlos', 'San Carlos'), ('San Rafael - El Placer', 'San Rafael - El Placer'), ('San Vicente', 'San Vicente'),
            ('Santa Monica', 'Santa Mónica'), ('Sauce de Portezuelo', 'Sauce de Portezuelo'), ('Solis', 'Solís'), ('Villa Delia', 'Villa Delia'),
        )),
        ('Montevideo', (
            ('Abayuba', 'Abayubá'), ('Aguada', 'Aguada'), ('Aires Puros', 'Aires Puros'), ('Arroyo Seco', 'Arroyo Seco'), ('Atahualpa', 'Atahualpa'),
            ('Bañados de Carrasco', 'Bañados de Carrasco'), ('Barrio Sur', 'Barrio Sur'), ('Bella Italia', 'Bella Italia'),
            ('Bella Vista', 'Bella Vista'), ('Belvedere', 'Belvedere'), ('Bolivar', 'Bolívar'), ('Brazo Oriental', 'Brazo Oriental'),
            ('Buceo', 'Buceo'), ('Capurro', 'Capurro'), ('Carrasco', 'Carrasco'), ('Carrasco Norte', 'Carrasco Norte'), ('Casabo', 'Casabó'),
            ('Casavalle', 'Casavalle'), ('Castro', 'Castro'), ('Centro', 'Centro'), ('Cerrito de la Victoria', 'Cerrito de la Victoria'),
            ('Ciudad Vieja', 'Ciudad Vieja'), ('Colon', 'Colón'), ('Colon Sureste, Abayuba', 'Colón Sureste, Abayubá'),
            ('Conciliacion', 'Conciliación'), ('Cordon', 'Cordón'), ('Flor de Maroñas', 'Flor de Maroñas'), ('Goes', 'Goes'),
            ('Ituzaingo', 'Ituzaingó'), ('Jacinto Vera', 'Jacinto Vera'), ('Jardines del Hipodromo', 'Jardines del Hipódromo'),
            ('La Blanqueada', 'La Blanqueada'), ('La Comercial', 'La Comercial'), ('La Figurita', 'La Figurita'), ('La Paloma Mvd', 'La Paloma'),
            ('La Teja', 'La Teja'), ('Larrañaga', 'Larrañaga'), ('Las Acacias', 'Las Acacias'), ('Las Canteras', 'Las Canteras'),
            ('Lavalleja', 'Lavalleja'), ('Lezica', 'Lezica'), ('Malvin', 'Malvín'), ('Malvin Norte', 'Malvín Norte'), ('Manga', 'Manga'),
            ('Manga Rural', 'Manga Rural'), ('Maroñas', 'Maroñas'), ('Melilla', 'Melilla'), ('Mercado Modelo', 'Mercado Modelo'),
            ('Nueva Savona', 'Nueva Savona'), ('Nuevo Paris', 'Nuevo París'), ('Pajas Blancas', 'Pajas Blancas'), ('Palermo', 'Palermo'),
            ('Parque Batlle', 'Parque Batlle'), ('Parque Guarani', 'Parque Guaraní'), ('Parque Rodo', 'Parque Rodó'),
            ('Paso de la Arena', 'Paso de la Arena'), ('Paso de las Duranas', 'Paso de las Duranas'), ('Paso Molino', 'Paso Molino'),
            ('Peñarol', 'Peñarol'), ('Perez Castellanos', 'Pérez Castellanos'), ('Piedras Blancas', 'Piedras Blancas'),
            ('Pocitos y La Mondiola', 'Pocitos y La Mondiola'), ('Prado', 'Prado'), ('Pueblo Victoria', 'Pueblo Victoria'),
            ('Punta Carretas', 'Punta Carretas'), ('Punta de Rieles', 'Punta de Rieles'), ('Punta Gorda', 'Punta Gorda'),
            ('Reducto', 'Reducto'), ('Retiro', 'Retiro'), ('Santiago Vazquez', 'Santiago Vázquez'), ('Sayago', 'Sayago'),
            ('Toledo Chico', 'Toledo Chico'), ('Tomkinson', 'Tomkinson'), ('Tres Cruces', 'Tres Cruces'), ('Tres Ombues', 'Tres Ombúes'),
            ('Union', 'Unión'), ('Villa del Cerro', 'Villa del Cerro'), ('Villa Dolores', 'Villa Dolores'), ('Villa Española', 'Villa Española'),
            ('Villa Garcia', 'Villa García'), ('Villa Muñoz', 'Villa Muñoz'),
        )),
        ('Paysandú', (
            ('Araujo', 'Araujo'), ('Arbolito', 'Arbolito'), ('Beisso', 'Beisso'), ('Bella Vista Pay', 'Bella Vista'),
            ('Cañada del Pueblo', 'Cañada del Pueblo'), ('Casablanca', 'Casablanca'), ('Cerro Chato', 'Cerro Chato'),
            ('Chacras de Paysandu', 'Chacras de Paysandú'), ('Chapicuy', 'Chapicuy'), ('Constancia', 'Constancia'),
            ('Cuchilla de Buricayupi', 'Cuchilla de Buricayupi'), ('Cuchilla de Fuego', 'Cuchilla de Fuego'), ('El Eucaliptus', 'El Eucaliptus'),
            ('Esperanza', 'Esperanza'), ('Estacion Porvenir', 'Estación Porvenir'), ('Gallinal', 'Gallinal'), ('Guichon', 'Guichón'),
            ('La Tentacion', 'La Tentación'), ('Lorenzo Geyres', 'Lorenzo Geyres'), ('Merinos', 'Merinos'), ('Morato', 'Morato'),
            ('Nuevo Paysandu', 'Nuevo Paysandú'), ('Orgoroso', 'Orgoroso'), ('Paysandu', 'Paysandú'), ('Piñera', 'Piñera'),
            ('Piedra Sola', 'Piedra Sola'), ('Piedras Coloradas', 'Piedras Coloradas'), ('Porvenir', 'Porvenir'), ('Pueblo Alonzo', 'Pueblo Alonzo'),
            ('Pueblo Federacion', 'Pueblo Federación'), ('Puntas de Arroyo Negro', 'Puntas de Arroyo Negro'), ('Quebracho', 'Quebracho'),
            ('Queguayar', 'Queguayar'), ('San Felix', 'San Félix'), ('Soto', 'Soto'), ('Tambores', 'Tambores'),
            ('Termas de Almiron', 'Termas de Almirón'), ('Termas de Guaviyu', 'Termas de Guaviyú'),
            ('Villa Maria (Tiatucura)', 'Villa María (Tiatucura)'), ('Zeballos', 'Zeballos'),
        )),
        ('Río Negro', (
            ('Algorta', 'Algorta'), ('Barrio Anglo', 'Barrio Anglo'), ('Bellaco', 'Bellaco'), ('El Ombu', 'El Ombú'), ('Fray Bentos', 'Fray Bentos'),
            ('Grecco', 'Grecco'), ('Las Cañas', 'Las Cañas'), ('Los Arrayanes', 'Los Arrayanes'), ('Menafra', 'Menafra'), ('Merinos', 'Merinos'),
            ('Nuevo Berlin', 'Nuevo Berlín'), ('Paso de los Mellizos', 'Paso de los Mellizos'), ('San Javier', 'San Javier'),
            ('Sarandi de Navarro', 'Sarandí de Navarro'), ('Tres Quintas', 'Tres Quintas'), ('Villa General Borges', 'Villa General Borges'),
            ('Villa Maria', 'Villa María'), ('Young', 'Young'),
        )),
        ('Rivera', (
            ('Amarillo', 'Amarillo'), ('Arroyo Blanco', 'Arroyo Blanco'), ('Cerrillada', 'Cerrillada'), ('Cerro Pelado', 'Cerro Pelado'),
            ('Cerros de la Calera', 'Cerros de la Calera'), ('La Pedrera Riv', 'La Pedrera'), ('Lagos del Norte', 'Lagos del Norte'),
            ('Lagunon', 'Lagunón'), ('Lapuente', 'Lapuente'), ('Las Flores', 'Las Flores'), ('Mandubi', 'Mandubí'), ('Masoller', 'Masoller'),
            ('Minas de Corrales', 'Minas de Corrales'), ('Moirones', 'Moirones'), ('Paso Ataques', 'Paso Ataques'),
            ('Paso Hospital', 'Paso Hospital'), ('Rivera', 'Rivera'), ('Santa Teresa', 'Santa Teresa'), ('Tranqueras', 'Tranqueras'),
            ('Vichadero', 'Vichadero'),
        )),
        ('Rocha', (
            ('18 de Julio', '18 de Julio'), ('19 de Abril', '19 de Abril'), ('Aguas Dulces', 'Aguas Dulces'), ('Arachania', 'Arachania'),
            ('Barra de Valizas', 'Barra de Valizas'), ('Barra del Chuy', 'Barra del Chuy'), ('Barrio Pereira', 'Barrio Pereira'),
            ('Barrio Torres', 'Barrio Torres'), ('Cabo Polonio', 'Cabo Polonio'), ('Capacho', 'Capacho'), ('Castillos', 'Castillos'),
            ('Cebollati', 'Cebollatí'), ('Chuy', 'Chuy'), ('La Aguada y Costa Azul', 'La Aguada y Costa Azul'), ('La Coronilla', 'La Coronilla'),
            ('La Esmeralda', 'La Esmeralda'), ('La Paloma', 'La Paloma'), ('La Pedrera', 'La Pedrera'), ('La Ribiera', 'La Ribiera'),
            ('Lascano', 'Lascano'), ('Oceania del Polonio', 'Oceanía del Polonio'), ('Palmares de La Coronilla', 'Palmares de La Coronilla'),
            ('Paralle', 'Paralle'), ('Pta. Rubia y Sta. Isabel de La Pedrera', 'Pta. Rubia y Sta. Isabel de La Pedrera'),
            ('Pueblo Nuevo', 'Pueblo Nuevo'), ('Puente Valizas', 'Puente Valizas'), ('Puerto de Los Botes', 'Puerto de Los Botes'),
            ('Puimayen', 'Puimayen'), ('Punta del Diablo', 'Punta del Diablo'), ('Rocha', 'Rocha'), ('San Antonio', 'San Antonio'),
            ('San Luis al Medio', 'San Luis al Medio'), ('Tajamares de La Pedrera', 'Tajamares de La Pedrera'), ('Velazquez', 'Velázquez'),
        )),
        ('Salto', (
            ('Albisu', 'Albisu'), ('Arenitas Blancas', 'Arenitas Blancas'), ('Belen', 'Belén'), ('Biassini', 'Biassini'),
            ('Campo de Todos', 'Campo de Todos'), ('Cayetano', 'Cayetano'), ('Celeste', 'Celeste'), ('Cerros de Vera', 'Cerros de Vera'),
            ('Chacras de Belen', 'Chacras de Belén'), ('Colonia 18 de Julio', 'Colonia 18 de Julio'), ('Colonia Itapebi', 'Colonia Itapebí'),
            ('Constitucion', 'Constitución'), ('Cuchilla de Guaviyu', 'Cuchilla de Guaviyú'), ('Fernandez', 'Fernández'),
            ('Garibaldi', 'Garibaldi'), ('Guaviyu de Arapey', 'Guaviyú de Arapey'), ('Las Flores', 'Las Flores'), ('Laureles', 'Laureles'),
            ('Lluveras', 'Lluveras'), ('Migliaro', 'Migliaro'), ('Olivera', 'Olivera'), ('Osimani y Llerena', 'Osimani y Llerena'),
            ('Palomas', 'Palomas'), ('Parque Jose Luis', 'Parque José Luis'), ('Paso Cementerio', 'Paso Cementerio'),
            ('Paso de Llas Piedras de Arerungua', 'Paso de Llas Piedras de Arerunguá'), ('Paso del Parque del Dayman', 'Paso del Parque del Daymán'),
            ('Puntas de Valentin', 'Puntas de Valentín'), ('Quintana', 'Quintana'), ('Rincon de Valentin', 'Rincón de Valentín'),
            ('Russo', 'Russo'), ('Salto', 'Salto'), ('San Antonio', 'San Antonio'), ('Sarandi de Arapey', 'Sarandí de Arapey'),
            ('Saucedo', 'Saucedo'), ('Termas del Arapey', 'Termas del Arapey'), ('Termas del Dayman', 'Termas del Daymán'),
        )),
        ('San José', (
            ('18 de Julio (Pueblo Nuevo)', '18 de Julio (Pueblo Nuevo)'), ('Boca del Cufre', 'Boca del Cufré'), ('Cañada Grande', 'Cañada Grande'),
            ('Capurro', 'Capurro'), ('Carreta Quemada', 'Carreta Quemada'), ('Ceramicas del Sur', 'Cerámicas del Sur'),
            ('Cololo Tinosa', 'Cololo Tinosa'), ('Colonia Delta', 'Colonia Delta'), ('Costas de Pereira', 'Costas de Pereira'),
            ('Delta del Tigre y Villas', 'Delta del Tigre y Villas'), ('Ecilda Paullier', 'Ecilda Paullier'), ('Gonzalez', 'González'),
            ('Ituzaingo', 'Ituzaingó'), ('Juan Soler', 'Juan Soler'), ('Kiyu-Ordeig', 'Kiyu-Ordeig'), ('La Boyada', 'La Boyada'),
            ('Libertad', 'Libertad'), ('Mal Abrigo', 'Mal Abrigo'), ('Mangrullo', 'Mangrullo'), ('Monte Grande', 'Monte Grande'),
            ('Playa Pascual', 'Playa Pascual'), ('Puntas de Valdez', 'Puntas de Valdez'), ('Radial', 'Radial'), ('Rafael Peraza', 'Rafael Peraza'),
            ('Raigon', 'Raigón'), ('Rincon del Pino', 'Rincón del Pino'), ('Rodriguez', 'Rodríguez'),
            ('Safici (Parque Postel)', 'Safici (Parque Postel)'), ('San Gregorio', 'San Gregorio'), ('San Jose de Mayo', 'San José de Mayo'),
            ('Santa Monica', 'Santa Mónica'), ('Scavino', 'Scavino'), ('Villa Maria', 'Villa María'),
        )),
        ('Soriano', (
            ('Agraciada Sor', 'Agraciada'), ('Cañada Nieto', 'Cañada Nieto'), ('Cardona', 'Cardona'), ('Castillos', 'Castillos'),
            ('Chacras de Dolores', 'Chacras de Dolores'), ('Colonia Concordia', 'Colonia Concordia'), ('Cuchilla del Perdido', 'Cuchilla del Perdido'),
            ('Dolores', 'Dolores'), ('Egaña', 'Egaña'), ('El Tala', 'El Tala'), ('Jose Enrique Rodo', 'José Enrique Rodó'),
            ('La Concordia', 'La Concordia'), ('La Loma', 'La Loma'), ('Lares', 'Lares'), ('Mercedes', 'Mercedes'), ('Palmar', 'Palmar'),
            ('Palmitas', 'Palmitas'), ('Palo Solo', 'Palo Solo'), ('Perseverano', 'Perseverano'), ('Risso', 'Risso'),
            ('Sacachispas', 'Sacachispas'), ('Santa Catalina', 'Santa Catalina'), ('Villa Soriano', 'Villa Soriano'),
        )),
        ('Tacuarembó', (
            ('Achar', 'Achar'), ('Ansina', 'Ansina'), ('Balneario Ipora', 'Balneario Iporá'), ('Cardozo', 'Cardozo'),
            ('Cerro de Pastoreo', 'Cerro de Pastoreo'), ('Chamberlain', 'Chamberlain'), ('Clara', 'Clara'),
            ('Cruz de los Caminos', 'Cruz de los Caminos'), ('Cuchilla de Peralta', 'Cuchilla de Peralta'), ('Cuchilla del Ombu', 'Cuchilla del Ombú'),
            ('Curtina', 'Curtina'), ('La Hilera', 'La Hilera'), ('La Pedrera Tac', 'La Pedrera'), ('Las Toscas', 'Las Toscas'),
            ('Laureles', 'Laureles'), ('Montevideo Chico', 'Montevideo Chico'), ('Paso Bonilla', 'Paso Bonilla'),
            ('Paso de Los Toros', 'Paso de Los Toros'), ('Paso del Cerro', 'Paso del Cerro'), ('Piedra Sola', 'Piedra Sola'),
            ('Pueblo de Arriba', 'Pueblo de Arriba'), ('Pueblo del Barro', 'Pueblo del Barro'), ('Punta de Carretera', 'Punta de Carretera'),
            ('Puntas de Cinco Sauces', 'Puntas de Cinco Sauces'), ('Rincon de Pereira', 'Rincón de Pereira'),
            ('Rincon del Bonete', 'Rincón del Bonete'), ('San Gregorio de Polanco', 'San Gregorio de Polanco'), ('Sauce de Batovi', 'Sauce de Batovi'),
            ('Tacuarembo', 'Tacuarembó'), ('Tambores', 'Tambores'),
        )),
        ('Treinta y Tres', (
            ('Arrocera Bonomo', 'Arrocera Bonomo'), ('Arrocera El Tigre', 'Arrocera El Tigre'), ('Arrocera La Catumbera', 'Arrocera La Catumbera'),
            ('Arrocera La Querencia', 'Arrocera La Querencia'), ('Arrocera Las Palmas', 'Arrocera Las Palmas'),
            ('Arrocera Los Ceibos', 'Arrocera Los Ceibos'), ('Arrocera Los Teros', 'Arrocera Los Teros'), ('Arrocera Mini', 'Arrocera Mini'),
            ('Arrocera Procipa', 'Arrocera Procipa'), ('Arrocera Rincon', 'Arrocera Rincón'), ('Arrocera San Fernando', 'Arrocera San Fernando'),
            ('Arrocera Santa Fe', 'Arrocera Santa Fe'), ('Arrocera Zapata', 'Arrocera Zapata'), ('Arrozal Treinta y Tres', 'Arrozal Treinta y Tres'),
            ('Cerro Chato', 'Cerro Chato'), ('Ejido de Treinta y Tres', 'Ejido de Treinta y Tres'), ('El Bellaco', 'El Bellaco'),
            ('Estacion Rincon', 'Estación Rincón'), ('Gral. Enrique Martinez', 'Gral. Enrique Martínez'),
            ('Isla Patrulla (Maria Isabel)', 'Isla Patrulla (María Isabel)'), ('Maria Albina', 'María Albina'),
            ('Mendizabal (El Oro)', 'Mendizabal (El Oro)'), ('Poblado Alonzo', 'Poblado Alonzo'), ('Puntas del Parao', 'Puntas del Parao'),
            ('Santa Clara de Olimar', 'Santa Clara de Olimar'), ('Treinta y Tres', 'Treinta y Tres'), ('Valentines', 'Valentines'),
            ('Vergara', 'Vergara'), ('Villa Passano', 'Villa Passano'), ('Villa Sara', 'Villa Sara'),
        )),
    )

    nombre = models.CharField(max_length=30)
    especie = models.CharField(max_length=2, choices=especies, default='')
    sexo = models.CharField(max_length=2, choices=sexos, default='', blank=True)   
    edad = models.CharField(max_length=2, choices=edades,default='', blank=True)
    tamaño = models.CharField(max_length = 2, choices = tamaños, default = '', null = True)
    color_1 = models.CharField(max_length=2, choices=colores, default='', blank=True, verbose_name='Color Primario')
    color_2 = models.CharField(max_length=2, choices=colores, default='', blank=True, verbose_name= 'Color Secundario')
    color_3 = models.CharField(max_length=2, choices=colores, default='', blank=True, verbose_name='Color Terciario')
    chapa_id = models.BooleanField(default=False, null=True)
    collar = models.BooleanField(default=False, null=True)
    collar_color = models.CharField(max_length=30, blank=True, null=True)
    marcas = models.TextField(blank=True, null=True)
    tipo_repo = models.CharField(max_length=2, choices=tipos_repos, default='', null=True)
    fecha_reportado = models.DateField(null=True)
    fecha_creado = models.DateTimeField(auto_now_add=True, null=True)
    fecha_modificado = models.DateTimeField(auto_now=True, null=True)
    comentarios = models.TextField(null=True)
    imagen = models.ImageField(verbose_name='Imagen', upload_to='media/fotos_animales', blank=True)

    departamento = models.CharField(max_length=2, choices = dptos, default = '', null=True)
    localidad = models.CharField(max_length=40, choices = localidades, default = '', null=True)
    calle = models.CharField(max_length=30, default='', null=True)
    esquina = models.CharField(max_length=30, default='', null=True)

    class Meta:
        ordering = ['nombre']

    usuario = models.ForeignKey(
        "Usuario",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.nombre


class Perro(Animal):
    razas = (('', 'Seleccionar'), ('DE', 'Desconocido'), ('CR', 'Cruza'), ('AF', 'Afgano'), ('AT', 'Airedale Terrier'),
         ('AK', 'Akita'), ('AL', 'Alano'), ('BH', 'Basset Hound'), ('BE', 'Beagle'), ('BF', 'Bichon Frise'),
         ('BL', 'Bloodhound'), ('BC', 'Border Collie'), ('BO', 'Boxer'), ('BB', 'Boyero de Berna'),
         ('BT', 'Bull Terrier'), ('BF', 'Bulldog Francés'), ('BI', 'Bulldog Inglés'), ('BU', 'Bullmastiff'),
         ('CN', 'Caniche'), ('CI', 'Cimarrón Uruguayo'), ('CA', 'Cocker Spaniel Americano'), 
         ('CI', 'Cocker Spaniel Inglés'), ('CL', 'Collie de pelo largo'), ('CH', 'Chihuahua'),
         ('CW', 'Chow Chow'), ('DH', 'Dachshund'), ('DM', 'Dálmata'), ('DO', 'Doberman'), ('DG', 'Dogo Argentino'),
         ('DB', 'Dogo de Burdeos'), ('FB', 'Fila Brasilero'), ('FT', 'Fox Terrier'),
         ('GG', 'Galgo'), ('GR', 'Golden Retriever'), ('GD', 'Gran Danés'), ('GH', 'Greyhound'), 
         ('HS', 'Husky Siberiano'), ('JR', 'Jack Russell Terrier'), ('LR', 'Labrador Retriever'),
         ('MN', 'Mastín Napolitano'), ('OA', 'Ovejero Alemán'), ('OB', 'Ovejero Belga'), ('PK', 'Pekinés'),
         ('PM', 'Pinscher miniatura'), ('PT', 'Pitbull'), ('PN', 'Pointer'), ('PM', 'Pomerania'),
         ('PD', 'Poodle'), ('PG', 'Pug'), ('RW', 'Rottweiler'), ('SB', 'San Bernardo'), ('SG', 'Schnauzer Gigante'),
         ('SM', 'Schnauzer miniatura'), ('SS', 'Schnauzer standard'), ('ST', 'Scottish Terrier'), 
         ('SI', 'Setter Inglés'), ('SR', 'Setter Irlandés'), ('SP', 'Shar Pei'), ('SL', 'Shetland'),
         ('ST', 'Shih Tzu'), ('SH', 'Siberian Husky'), ('TN', 'Terranova'), ('WM', 'Weimaraner'), ('WP', 'Whippet'), 
         ('YT', 'Yorkshire Terrier'), ('OT', 'Otro'),)
    pelo_largos = (('','Seleccionar'), ('CO', 'Corto'), ('ME', 'Medio'), ('LA', 'Largo'), ('NA', 'No aplica'),)
    pelo_tipos = (('','Seleccionar'), ('LI', 'Liso'), ('AL', '"Alambre"'), ('EN', 'Enrulado'), ('NA', 'No aplica'),)


    raza = models.CharField(max_length=30, choices=razas, default='', null=True)
    pelo_largo = models.CharField(max_length=2, choices=pelo_largos, default='', null=True, verbose_name='Largo del pelo')
    pelo_tipo = models.CharField(max_length=2, choices=pelo_tipos, default='', null=True, verbose_name='Tipo de pelo')
    #pass

    def __str__(self):
        return self.nombre # + '  *  ' + self.raza


class Gato(Animal):
    razas = (('', 'Seleccionar'), ('CR', 'Cruza'), ('AN', 'Angora'), ('BI', 'Birmano'), ('PE', 'Persa'), ('RU', 'Ruso Azul'), ('SI', 'Siamés'), ('OT','Otro'),)
    pelo_largos = (('', 'Seleccionar'), ('CO', 'Corto'), ('ME', 'Medio'), ('LA', 'Largo'), ('NA', 'No aplica'),)
    pelo_tipos = (('', 'Seleccionar'), ('LI', 'Liso'), ('AL', '"Alambre"'), ('EN', 'Enrulado'), ('NA', 'No aplica'),)

    raza = models.CharField(max_length=30, choices = razas, default = '', null=True)
    pelo_largo = models.CharField(max_length=2, choices=pelo_largos, default='', null=True)
    pelo_tipo = models.CharField(max_length=2, choices=pelo_tipos, default='', null=True)
    #pass

    def __str__(self):
        return self.nombre # + '  *  ' + self.raza


class Otro(Animal):
    razas = (('', 'Seleccionar'), ('AV', 'Ave'), ('CB', 'Caballo'), ('CR', 'Cerdo'), ('CN', 'Conejo'),
             ('HM', 'Hámster'), ('SR', 'Serpiente'), ('TR', 'Tortuga'), ('OT', 'Otro'),)

    raza = models.CharField(max_length=30, choices=razas, default='', null=True)

    def __str__(self):
        return self.nombre + ', ' + self.raza


class Usuario(models.Model):
    apodo = models.CharField(max_length=10, null=True)
    contraseña = models.CharField(max_length=15, null=True)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    telefono = models.CharField(max_length=15,  verbose_name='Teléfono')
    email = models.CharField(max_length=50, unique=True,  verbose_name='Correo-e')
    mostrar_tel = models.BooleanField(default=False)
    mostrar_email = models.BooleanField(default=False)
    
    def __str__(self):
        return self.nombre + ' ' + self.apellido + ' ' + self.telefono

    class Meta:
        ordering = ['apellido']
