from django.http import HttpResponse
from django.conf import settings
from django.shortcuts import render
from core.models import Animal
from core.models import Perro
from core.models import Gato
from core.models import Otro


   
def registro(request):
    return render(request, 'registro.html')


def reporte(request):
    #el if se asegura que el usuario hizo clic sobre el boton ENVIAR
    if request.method == 'POST':

        #se crean variables y se igualan a las bases de datos
        animal = Animal()
        perro = Perro()
        gato = Gato()
        otro = Otro()

        #insertar los datos
        if request.POST.get('especie') == 'PE':
            perro.tipo_repo = request.POST.get('tipo_repo')
            perro.fecha_reportado = request.POST.get('fecha_reportado')
            perro.departamento = request.POST.get('departamento')
            perro.localidad = request.POST.get('localidad')
            perro.calle = request.POST.get('calle')
            perro.esquina = request.POST.get('esquina')
            perro.nombre = request.POST.get('nombre')
            perro.especie = request.POST.get('especie')
            perro.raza = request.POST.get('raza')
            perro.tamaño = request.POST.get('tamaño')
            perro.edad = request.POST.get('edad')
            perro.sexo = request.POST.get('sexo')
            perro.color_1 = request.POST.get('color_1')
            perro.color_2 = request.POST.get('color_2')
            perro.color_3 = request.POST.get('color_3')
            perro.pelo_largo = request.POST.get('pelo_largo')
            perro.pelo_tipo = request.POST.get('pelo_tipo')
            perro.marcas = request.POST.get('marcas')
            if request.POST.get('chapa_id') == 'on':
                perro.chapa_id = True
            else:
                perro.chapa_id = False
            if request.POST.get('collar') == 'on':
                perro.collar = True
            else:
                perro.collar = False
            perro.collar_color = request.POST.get('collar_color')
            perro.comentarios = request.POST.get('comentarios')
            perro.imagen = request.POST.get('imagen')

        elif request.POST.get('especie') == 'GA':
            gato.tipo_repo = request.POST.get('tipo_repo')
            gato.fecha_reportado = request.POST.get('fecha_reportado')
            gato.departamento = request.POST.get('departamento')
            gato.localidad = request.POST.get('localidad')
            gato.calle = request.POST.get('calle')
            gato.esquina = request.POST.get('esquina')
            gato.nombre = request.POST.get('nombre')
            gato.especie = request.POST.get('especie')
            gato.raza = request.POST.get('raza')
            gato.tamaño = request.POST.get('tamaño')
            gato.edad = request.POST.get('edad')
            gato.sexo = request.POST.get('sexo')
            gato.color_1 = request.POST.get('color_1')
            gato.color_2 = request.POST.get('color_2')
            gato.color_3 = request.POST.get('color_3')
            gato.pelo_largo = request.POST.get('pelo_largo')
            gato.pelo_tipo = request.POST.get('pelo_tipo')
            gato.marcas = request.POST.get('marcas')
            if request.POST.get('chapa_id') == 'on':
                gato.chapa_id = True
            else:
                gato.chapa_id = False
            if request.POST.get('collar') == 'on':
                gato.collar = True
            else:
                gato.collar = False
            gato.collar_color = request.POST.get('collar_color')
            gato.comentarios = request.POST.get('comentarios')
            gato.imagen = request.POST.get('imagen')

        else:
            otro.tipo_repo = request.POST.get('tipo_repo')
            otro.fecha_reportado = request.POST.get('fecha_reportado')
            otro.departamento = request.POST.get('departamento')
            otro.localidad = request.POST.get('localidad')
            otro.calle = request.POST.get('calle')
            otro.esquina = request.POST.get('esquina')
            otro.nombre = request.POST.get('nombre')
            otro.especie = request.POST.get('especie')
            otro.raza = request.POST.get('raza')
            otro.tamaño = request.POST.get('tamaño')
            otro.edad = request.POST.get('edad')
            otro.sexo = request.POST.get('sexo')
            otro.color_1 = request.POST.get('color_1')
            otro.color_2 = request.POST.get('color_2')
            otro.color_3 = request.POST.get('color_3')
            otro.marcas = request.POST.get('marcas')
            if request.POST.get('chapa_id') == 'on':
                otro.chapa_id = True
            else:
                otro.chapa_id = False
            if request.POST.get('collar') == 'on':
                otro.collar = True
            else:
                otro.collar = False
            otro.collar_color = request.POST.get('collar_color')
            otro.comentarios = request.POST.get('comentarios')
            otro.imagen = request.POST.get('imagen')

        #salvar los datos
        animal.save()
        perro.save()
        gato.save()
        otro.save()

        #devolver un template avisando que se generó el reporte
        return render(request, "reporte_exito.html")

    return render(request, "reporte.html")


def reporte_exito(request):
    return render(request, 'reporte_exito.html')

def reporte_ver(request):
    return render(request, 'reporte_ver.html')

def ingreso(request):
    return render(request, 'ingreso.html')
